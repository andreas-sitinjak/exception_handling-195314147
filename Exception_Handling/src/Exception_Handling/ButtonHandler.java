package Exception_Handling;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;

public class ButtonHandler implements ActionListener {

    public ButtonHandler() {

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JButton clickedbutton = (JButton) event.getSource();
        JRootPane rootpane = clickedbutton.getRootPane();
        Frame frame = (JFrame) rootpane.getParent();
        String buttonText = clickedbutton.getText();

        frame.setTitle("You Clicked" + buttonText);
    }

}
