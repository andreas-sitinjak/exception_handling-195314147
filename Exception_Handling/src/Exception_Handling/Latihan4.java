package Exception_Handling;

import java.util.Scanner;

public class Latihan4 {

    public static void main(String[] args) {
        Scanner data = new Scanner(System.in);
        int Bil1, Bil2, hasil;
        while (true) {
            try {
                System.out.println("MAsukkan Bilangan 1 : ");
                Bil1 = data.nextInt();
                break;
            } catch (Exception e) {
                System.out.println("Terjadi Kesalahan input, Mohon diulangi!");
                data.nextLine();
            }
        }
        while (true) {
            try {
                System.out.println("Masukkan Bilangan 2 ");
                Bil2 = data.nextInt();
                break;
            } catch (Exception e) {
                System.out.println("Terjadi Kesalahan input, Mohon diulangi!");
                data.nextLine();
            }
        }
        hasil = Bil1 + Bil2;
        System.out.println("Hasil Penjumlahan Kedua Bilangan itu :  " + hasil);
    }
}
