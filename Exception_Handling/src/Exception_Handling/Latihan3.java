package Exception_Handling;

import javax.swing.JOptionPane;

public class Latihan3 {

    public static void main(String[] args) {
        int age = 0;
        String inputStr = JOptionPane.showInputDialog(null, "Masukkan umur anda : ");
        try {
            age = Integer.parseInt(inputStr);
        } catch (Exception e) {
            age = 0;
        }
        System.out.println("Umur Anda: " + age + " tahun");
    }
}
