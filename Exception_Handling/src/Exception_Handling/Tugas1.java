package Exception_Handling;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

public class Tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    JTextField P, L, Luas;
    private JButton hitung;

    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
    }

    public Tugas1() {
        Container contentpane = getContentPane();
        contentpane.setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("LUAS TANAH");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        P = new JTextField();
        P.setBounds(120, 20, 150, 30);
        contentpane.add(P);

        L = new JTextField();
        L.setBounds(120, 60, 150, 30);
        contentpane.add(L);

        Luas = new JTextField();
        Luas.setEditable(false);
        Luas.setBounds(120, 100, 150, 30);
        contentpane.add(Luas);

        JLabel label1 = new JLabel("Panjang (m)");
        label1.setBounds(20, 20, 80, 30);
        contentpane.add(label1);

        JLabel label2 = new JLabel("Lebar (m)");
        label2.setBounds(20, 60, 80, 30);
        contentpane.add(label2);

        JLabel label3 = new JLabel("Luas (m) ");
        label3.setBounds(20, 100, 80, 30);
        contentpane.add(label3);

        hitung = new JButton("Hitung");
        hitung.setBounds(100, 150, 100, 30);
        contentpane.add(hitung);

        ButtonHandler handler = new ButtonHandler();
        hitung.addActionListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent a) {
        try {
            int panjang = Integer.parseInt(P.getText());
            int lebar = Integer.parseInt(L.getText());
            int luas = panjang * lebar;
            Luas.setText(Integer.toString(luas));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Maaf,hanya integer yang diperbolehkan!", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

}
